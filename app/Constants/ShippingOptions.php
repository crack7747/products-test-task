<?php


namespace App\Constants;

final class ShippingOptions
{
    const FREE_STANDARD = 1;
    const EXPRESS = 2;

    const PRICES = [
        self::FREE_STANDARD => 0,
        self::EXPRESS => 10,
    ];

    /**
     * @param  int  $option
     * @return float
     */
    public static function getPrice(int $option): float
    {
        return self::PRICES[$option];
    }
}
