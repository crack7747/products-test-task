<?php

namespace App\Models;

use App\Constants\ShippingOptions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    /**
     * @param  array  $cart_products
     * @param  array  $client_info
     * @return $this
     */
    function fillFromCart(array $cart_products, array $client_info): Order
    {
        $this->client_name = $client_info['name'];
        $this->client_address = $client_info['address'];

        $shipping_option = $client_info['shipping_option'];
        $this->total_shipping_value = ShippingOptions::getPrice($shipping_option);

        $product_ids = collect($cart_products)->pluck('id');
        $products = Product::whereIn('id', $product_ids)->get();
        $this->total_product_value = $products->reduce(function ($a, $i) {
            return $i->price + $a;
        });
        return $this;
    }
}
