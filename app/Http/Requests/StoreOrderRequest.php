<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_info.name' => 'required|min:1|max:255',
            'client_info.address' => 'required|min:3|max:255',
            'client_info.shipping_option' => 'required|integer',
            'card_info.number' => 'required|size:12',
            'card_info.expiration_month' => 'required|integer|min:1|max:12',
            'card_info.expiration_year' => 'required|integer|min:1900|max:2100',
            'card_info.cvv' => 'required|integer|digits_between:3,4',
            'cart_products.*.id' => 'required|integer|exists:products,id',
        ];
    }

    public function attributes()
    {
        return [
            'client_info.name' => 'Name',
            'client_info.address' => 'Address',
            'client_info.shipping_option' => 'Shipping option',
            'card_info.number' => 'Card number',
            'card_info.expiration_month' => 'Card expiration month',
            'card_info.expiration_year' => 'Card expiration year',
            'card_info.cvv' => 'Card CVV',
        ];
    }
}
