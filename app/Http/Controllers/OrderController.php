<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderRequest;
use App\Models\Order;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    /**
     * @param  StoreOrderRequest  $request
     * @return JsonResponse
     */
    public function store(StoreOrderRequest $request): JsonResponse
    {
        $order = new Order();
        $order->fillFromCart($request->cart_products, $request->client_info)->save();
        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
