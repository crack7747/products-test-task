import Vue from 'vue'
import VueRouter from 'vue-router';

import axios from 'axios'
import VueAxios from 'vue-axios'

import router from './router/index';
import store from './store/index';
import App from './app.vue';

import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';

Vue.use(VueToast);
Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(VueAxios, axios)


new Vue({
    router,
    store,
    render: h => h(App),
    created() {
        this.$router.push('/')
    }
}).$mount('#app')
