import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        cart_products: [],
        client_info: {
            name: null,
            address: null,
            shipping_option: null
        },
    },
    mutations: {
        addProduct (state, item) {
            if(!state.cart_products.some(i => i.id === item.id)){
                state.cart_products.push(item);
            }

        },
        removeProduct (state, item) {
            state.cart_products = state.cart_products.filter(i => i.id !== item.id)
        },
        saveClientInfo (state, item) {
            state.client_info = item;
        }
    },
    actions: {}
})

export default store;
