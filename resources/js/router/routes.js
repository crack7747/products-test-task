const routes = [
    {
        path: '',
        component: () => import('../pages/home.vue'),
        name: 'home'
    },
    {
        path: 'checkout',
        component: () => import('../pages/checkout.vue'),
        name: 'checkout'
    },
    {
        path: 'payment',
        component: () => import('../pages/payment.vue'),
        name: 'payment'
    },
    {
        path: 'success',
        component: () => import('../pages/success.vue'),
        name: 'success'
    },
]
export default routes;
